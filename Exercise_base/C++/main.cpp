#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

/// \brief ImportText import the text for encryption
/// \param inputFilePath: the input file path
/// \param text: the resulting text
/// \return the result of the operation, true is success, false is error
bool ImportText(const string& inputFilePath,
                string& text);

/// \brief Encrypt encrypt the text
/// \param text: the text to encrypt
/// \param password: the password for encryption
/// \param encryptedText: the resulting encrypted text
/// \return the result of the operation, true is success, false is error
bool Encrypt(const string& text,
             const string& password,
             string& encryptedText);

/// \brief Decrypt decrypt the text
/// \param text: the text to decrypt
/// \param password: the password for decryption
/// \param decryptedText: the resulting decrypted text
/// \return the result of the operation, true is success, false is error
bool Decrypt(const string& text,
             const string& password,
             string& decryptedText);

int main(int argc, char** argv)
{
  if (argc < 2)
  {
    cerr<< "Password shall passed to the program"<< endl;
    return -1;
  }
  string password = argv[1];

  string inputFileName = "./text.txt", text;
  if (!ImportText(inputFileName, text))
  {
    cerr<< "Something goes wrong with import"<< endl;
    return -1;
  }
  else
    cout<< "Import successful: result= "<< text<< endl;

  string encryptedText;
  if (!Encrypt(text, password, encryptedText))
  {
    cerr<< "Something goes wrong with encryption"<< endl;
    return -1;
  }
  else
    cout<< "Encryption successful: result= "<< encryptedText<< endl;

  text = encryptedText;

  string decryptedText;
  if (!Decrypt(text, password, decryptedText))
  {
    cerr<< "Something goes wrong with decryption"<< endl;
    return -1;
  }
  else
    cout<< "Decryption successful: result= "<< decryptedText<< endl;

  return 0;
}

bool ImportText(const string& inputFilePath,
                string& text)
{
    ifstream file;
    file.open(inputFilePath);

    getline(file, text);

    cout << text << endl;
    file.close();
    return true;
}

bool Encrypt(const string& text,
             const string& password,
             string& encryptedText)
{
    int lengthOfPassword = password.size();
    int lengthOfText = text.size();

    char * temporaryContainer = new char[lengthOfText];

    for(int i = 0; i < lengthOfText; i++)
        {
            char a1 = text[i], a2 = password[i%lengthOfPassword], a3 = a1 + a2;
            temporaryContainer[i] = a3;
        }
    encryptedText = temporaryContainer;

    delete [] temporaryContainer;

    return true;
}

bool Decrypt(const string& text,
             const string& password,
             string& decryptedText)
{
    int lengthOfPassword = password.size();
    int lengthOfText = text.size();

    char * temporaryContainer = new char[lengthOfText];

    for(int i = 0; i < lengthOfText; i++)
        {
            char a1 = text[i], a2 = password[i%lengthOfPassword], a3 = a1 - a2;
            temporaryContainer[i] = a3;
        }
    decryptedText = temporaryContainer;

    delete [] temporaryContainer;

    return  true;
}
