import sys

#PER UNA RAGIONE CHE NON CONOSCO, SU PYCHARM DEVO TRATTARE "text" COME MATRICE 1 PER n
#E DEVO LAVORARE CON "text[0][i]" E "password" COME MATRICE n PER 1 E DEVO LAVORARE CON "password[i][0]"

# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText(inputFilePath):
    file = open(inputFilePath, 'r')
    text = file.readlines()[0]
    file.close()
    return True, text


# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt(text, password):
    length_of_text = len(text)
    length_of_password = len(password)

    new_text = chr(ord(text[0]) + ord(password[0][0]))

    for i in range(1, length_of_text):
        new_text += (chr(ord(text[i]) + ord(password[i % length_of_password][0])))

    return True, new_text


# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(text, password):
    length_of_text = len(text)
    length_of_password = len(password)

    new_text = chr(ord(text[0]) - ord(password[0][0]))

    for i in range(1, length_of_text):
        new_text += (chr(ord(text[i]) - ord(password[i % length_of_password][0])))

    return True, new_text


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", *text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", *encryptedText)

    text: str = encryptedText

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    text = decryptedText

    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", *decryptedText)
