import math


class Point:
    def __init__(self, x: float, y: float):
        self.__px = x
        self.__py = y

    def getx(self) -> float:
        return self.__px

    def gety(self) -> float:
        return self.__py


class IPolygon:
    def area(self) -> float:
        pass


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int) -> None:
        x = center.getx()
        y = center.gety()
        self.__a = a
        self.__b = b

    def area(self):
        return math.pi * self.__a * self.__b


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        super(Circle, self).__init__(center, radius, radius)


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.__p1 = p1
        self.__p2 = p2
        self.__p3 = p3

    def area(self) -> float:
        self.__p1x = Point.getx(self.__p1)
        self.__p1y = Point.gety(self.__p1)
        self.__p2x = Point.getx(self.__p2)
        self.__p2y = Point.gety(self.__p2)
        self.__p3x = Point.getx(self.__p3)
        self.__p3y = Point.gety(self.__p3)
        S = math.fabs((self.__p1x * (self.__p2y - self.__p3y) + self.__p2x * (self.__p3y - self.__p1y) + self.__p3x * (self.__p1y - self.__p2y)) / 2)
        return S


class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int):
        super(TriangleEquilateral, self).__init__(p1,p1,p1)
        self.p1 = p1
        self.side = edge

    def area(self) -> float:
        return (math.sqrt(3) * self.side ** 2) / 4


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.__p1 = p1
        self.__p2 = p2
        self.__p3 = p3
        self.__p4 = p4

    def area(self) -> float:
        self.__p1x = Point.getx(self.__p1)
        self.__p1y = Point.gety(self.__p1)
        self.__p2x = Point.getx(self.__p2)
        self.__p2y = Point.gety(self.__p2)
        self.__p3x = Point.getx(self.__p3)
        self.__p3y = Point.gety(self.__p3)
        self.__p4x = Point.getx(self.__p4)
        self.__p4y = Point.gety(self.__p4)
        S1 = math.fabs((self.__p1x * (self.__p2y - self.__p3y) + self.__p2x * (self.__p3y - self.__p1y) + self.__p3x * (
                    self.__p1y - self.__p2y)) / 2)
        S2 = math.fabs((self.__p1x * (self.__p3y - self.__p4y) + self.__p3x * (self.__p4y - self.__p1y) + self.__p4x * (
                self.__p1y - self.__p3y)) / 2)
        return S1 + S2


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        super(Parallelogram, self).__init__(p1, p2, p4, p4)
        self.__p1 = p1
        self.__p2 = p2
        self.__p4 = p4

    def area(self) -> float:
        self.__p1x = Point.getx(self.__p1)
        self.__p1y = Point.gety(self.__p1)
        self.__p2x = Point.getx(self.__p2)
        self.__p2y = Point.gety(self.__p2)
        self.__p4x = Point.getx(self.__p4)
        self.__p4y = Point.gety(self.__p4)
        S = math.fabs((self.__p1x * (self.__p2y - self.__p4y) + self.__p2x * (self.__p4y - self.__p1y) + self.__p4x * (
                    self.__p1y - self.__p2y)) / 2)
        return S * 2

class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        super(Rectangle, self).__init__(p1, p1, p1)
        self.__base = base
        self.__height = height

    def area(self) -> float:
        return self.__base * self.__height

class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        super(Square, self).__init__(p1, edge, edge)
        super(Square, self).area()