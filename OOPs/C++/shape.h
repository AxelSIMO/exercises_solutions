#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>

using namespace std;

namespace ShapeLibrary {

  class Point {
      double px;
      double py;

    public:
      double getPx(void) const;
      double getPy(void) const;

      Point(const double& x,
            const double& y);
      Point(const Point& point);
      ~Point();
  };

  class IPolygon {
    public:
      virtual double Area() = 0;
  };

  class Ellipse : public IPolygon
  {
    protected:
      Point CenterPoint;
      ///double xCenterPoint;
      ///double yCenterPoint;
      int aAxis;
      int bAxis;
    public:
      using IPolygon::Area;
      Ellipse(const Point& center,
              const int& a,
              const int& b);

      double Area();
  };

  class Circle : public Ellipse
  {
    public:
      using Ellipse::Area;
      Circle(const Point& center,
             const int& radius);
  };


  class Triangle : public IPolygon
  {
    protected:
      Point _p1;
      Point _p2;
      Point _p3;

    public:
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      double Area();
  };


  class TriangleEquilateral : public Triangle
  {
      int triangleEdge;
    public:
      using Triangle::Area;
      TriangleEquilateral(const Point& p1,
                          const int& edge);
      double Area();
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      Point _p1;
      Point _p2;
      Point _p3;
      Point _p4;
    public:
      using IPolygon::Area;
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);

      double Area();
  };


  class Parallelogram : public Quadrilateral
  {
    public:
      using Quadrilateral::Area;
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4);
      double Area();
  };

  class Rectangle : public Parallelogram
  {
      int rectangleBase;
      int rectangleHeight;
    public:
      using Parallelogram::Area;
      Rectangle(const Point& p1,
                const int& base,
                const int& height);

      double Area();
  };

  class Square: public Rectangle
  {
    public:
      using Rectangle::Area;
      Square(const Point& p1,
             const int& edge);
  };
}

#endif // SHAPE_H
