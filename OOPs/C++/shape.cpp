#include "shape.h"
#include <math.h>

const double PI = 3.14159265358979323846;

namespace ShapeLibrary {

double Point::getPx() const
{
    return px;
}

double Point::getPy() const
{
    return py;
}

Point::Point(const double &x, const double &y)
{
    px = x;
    py = y;
}

Point::Point(const Point &point)
{
    px = point.px;
    py = point.py;

}

Point::~Point()
{
}

Ellipse::Ellipse(const Point &center, const int &a, const int &b) : CenterPoint(0,0)
{
    Point Point(center);
    aAxis = a;
    bAxis = b;
}

double Ellipse::Area()
{
    return PI * aAxis * bAxis;
}

Circle::Circle(const Point &center, const int &radius) : Ellipse(center, radius, radius)
{}

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3): _p1(p1), _p2(p2), _p3(p3)
{}

double Triangle::Area()
{
    double p1x = _p1.getPx();
    double p1y = _p1.getPy();

    double p2x = _p2.getPx();
    double p2y = _p2.getPy();

    double p3x = _p3.getPx();
    double p3y = _p3.getPy();

    return fabs((p1x * (p2y - p3y) + p2x * (p3y - p1y) + p3x * (p1y - p2y)) / 2);
}

TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge):Triangle(p1,p1,p1)
{
    triangleEdge = edge;
}

double TriangleEquilateral::Area()
{
    double height = sqrt(triangleEdge*triangleEdge - triangleEdge*triangleEdge / 4);
    double area = triangleEdge*height/2;
    return area;
}

Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4):_p1(p1), _p2(p2), _p3(p3),_p4(p4)
{}

double Quadrilateral::Area() ///Dividere in due triangoli e usare la formula dell'area del triangolo per ciascuno
{
    double p1x = _p1.getPx();
    double p1y = _p1.getPy();

    double p2x = _p2.getPx();
    double p2y = _p2.getPy();

    double p3x = _p3.getPx();
    double p3y = _p3.getPy();

    double p4x = _p4.getPx();
    double p4y = _p4.getPy();

    double S1 = fabs((p1x * (p2y - p3y) + p2x * (p3y - p1y) + p3x * (p1y - p2y)) / 2);
    double S2 = fabs((p1x * (p3y - p4y) + p3x * (p4y - p1y) + p4x * (p1y - p3y)) / 2);

    return S1 + S2;
}

Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4):Quadrilateral(p1, p2, p4, p4)
{}

double Parallelogram::Area()  ///Dividere in due triangoli e usare la formula dell'area del triangolo * 2
{
    double p1x = _p1.getPx();
    double p1y = _p1.getPy();

    double p2x = _p2.getPx();
    double p2y = _p2.getPy();

    double p4x = _p4.getPx();
    double p4y = _p4.getPy();

    return 2 * fabs((p1x * (p2y - p4y) + p2x * (p4y - p1y) + p4x * (p1y - p2y)) / 2);
}

Rectangle::Rectangle(const Point &p1, const int &base, const int &height):Parallelogram(p1,p1,p1)
{
    rectangleBase = base;
    rectangleHeight = height;
}

double Rectangle::Area()
{
    return rectangleBase * rectangleHeight;
}

Square::Square(const Point &p1, const int &edge):Rectangle(p1,edge,edge)
{}

}
